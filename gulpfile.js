var gulp = require('gulp');
var less = require('gulp-less');

// Compile LESS files from /less into /css
gulp.task('less', function() {
    return gulp.src('public/less/style.less')
        .pipe(less())
        .pipe(gulp.dest('public/css'))
});

// Dev task with browserSync
gulp.task('dev', ['less'], function() {
    gulp.watch('less/*.less', ['less']);
});