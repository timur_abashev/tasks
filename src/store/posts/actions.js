import postsData from './../data.json';

const Posts = {
    all() {
        return postsData;
    }
};

export default Posts;