import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux'
import { BrowserRouter as Router } from 'react-router-dom'

import App from './component/App/App';
import store from './store'

import registerServiceWorker from './base/registerServiceWorker';

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App />
        </Router>
    </Provider>
    , document.getElementById('gridder-wrapper'));

registerServiceWorker();
