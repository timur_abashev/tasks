import React, { Component} from 'react';
import { connect } from 'react-redux';
import Posts from './../../store/posts/actions';
import { GridderViewer, gridder } from '../../base/gridder';

class Grid1 extends Component {
    render() {
        let posts = Posts.all();

        return (
            <WrapList posts={posts}/>
        );
    }
}

class WrapList extends Component {
    componentDidMount() {
        gridder.dispatch({
            cellSize: 4,
            colCount: 24,
            rowsCount: 1000,
            breakpoint: 960
        });

        if ( gridder.storage.breakpoint ){
            gridder.storage.breakpoint = 960;
        }

        if ( gridder.storage.cellSize ){
            gridder.storage.cellSize = 4;
        }

        if ( gridder.storage.colCount ){
            gridder.storage.colCount = 24;
        }

        if ( gridder.storage.rowsCount ){
            gridder.storage.rowsCount = 1000;
        }

        gridder.renderGrid();

        gridder.eventer.on('resizeWindow.pageGrid1', gridder.renderGrid);
    }
    render() {
        let {posts} = this.props;

        return (
            <div className="gridder-post-grid">
                {posts.map((post) => (
                    <WrapListItem key={post.id} data={post} id={post.id}/>
                ))}
            </div>
        );
    }
}

class WrapListItem extends Component {
    constructor(props){
        super(props);

        this.handlerClick = this.handlerClick.bind(this);
    }
    handlerClick() {
        let props = this.props;

        new GridderViewer({
            data: {
                type: 'photo',
                src: props.data.photo_img,
                id: props.id,
                gridId: 1,
                mode: GridderViewer.VIEWER_MODE_VERTICAL
            }
        });
    }
    render() {
        let props = this.props;

        return (
            <div key={props.id} className={'gridder-post photo w-' + props.data.w + ' h-' + props.data.h} onClick={this.handlerClick}>
                <div className="gridder-post-item">
                    <img src={props.data.photo_img} key={props.id} alt="" />
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    isLoading: 0,
    posts: state.posts
});

export default connect(mapStateToProps, { })(Grid1);
