import React from 'react';
import { Link } from 'react-router-dom'

export const MyMenu = () => (
    <div className="gridder-navbar nclear">
        <div className="gridder-container">
            <Link to="/grid1" className="gridder-navbar-item">Grid1</Link>
            <Link to="/grid2" className="gridder-navbar-item">Grid2</Link>
        </div>
    </div>
);


export default MyMenu;