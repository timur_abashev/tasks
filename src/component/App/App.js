import React, { Component } from 'react';

import Routes from '../../base/Routes';

import { connect } from 'react-redux';

import Menu from './Menu';

class App extends Component {
    render() {
        return (
            <div>
                <Menu/>
                <div className="gridder-container">
                    <Routes/>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    isLoading: 0,
    posts: state.posts
});

export default connect(mapStateToProps, { })(App);