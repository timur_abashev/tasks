import React, {Component} from 'react';
import { Route, Switch } from 'react-router-dom';

import Grid1 from './../component/Grid1';
import Grid2 from './../component/Grid2';

class Routes extends Component {
    render() {
        return (
            <Switch>
                <Route path="/grid1" component={Grid1} />
                <Route path="/grid2" component={Grid2} />
            </Switch>
        );
    }
}

export default Routes;