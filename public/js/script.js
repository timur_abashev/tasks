var gridder = window.gridder || {};

/**
 * Event Machine
 * 22.03.2013
 *
 * new Eventer();
 */

var Eventer = function (params) {
    $.extend(this, {
        eventPrefix: '__em_'
    }, params);
};

(function (proto) {

    proto.on = function (type, fn) {
        var self = this;
        $(this).on(self.eventPrefix + type, function (e, data) {
            fn.call(self, { type: e.type }, data);
        });
        return this;
    };

    proto.emit = function (type, data) {
        $(this).triggerHandler(this.eventPrefix + type, data);
        return this;
    };

    proto.off = function (type) {
        $(this).off(this.eventPrefix + type);
        return this;
    };

})(Eventer.prototype);

(function(gridder){
    var storage = gridder.storage = gridder.storage || {};
    var eventer = gridder.eventer = gridder.eventer || new Eventer({});

    gridder.dispatch = function(params){
        initNodes();
        saveToStorage(params);
        bindGlobalEvents();
        calculateEventsData();
    };

    var saveToStorage = function(params){
        $.extend(true, storage, params);
    };

    var initNodes = function(){
        gridder.nodes = {
            $window: $(window),
            $wrapper: $('.gridder-wrapper'),
            $heap: $('#gridder-heap')
        };
    };

    var bindGlobalEvents = function(){
        gridder.nodes.$window
            .unbind('scroll.gridder')
            .bind('scroll.gridder', onScroll)
            .unbind('resize.gridder')
            .bind('resize.gridder', onResize)
            .unbind('move.gridder')
            .bind('move.gridder', onMove);
    };

    var calcPositionMouse = function(event){
        if ( event ){
            storage.pageX = event.pageX;
            storage.pageY = event.pageY;
        } else {
            storage.pageX = 0;
            storage.pageY = 0;
        }
    };

    var calcWindowScrollTop = function(){
        storage.scroolTop = $(window).scrollTop();
    };

    var calcWindowParams = function(){
        storage.windowWidth = $( window ).outerWidth();
        storage.windowHeight = $( window ).outerHeight();
    };

    var onMove = function(event){
        calcPositionMouse(event);

        eventer.emit('onMove');
    };

    var onScroll = function(){
        calcWindowScrollTop();

        eventer.emit('scrollWindow');
    };

    var onResize = function(){
        calcWindowParams();

        eventer.emit('resizeWindow');
    };

    var calculateEventsData = function(){
        calcPositionMouse();
        calcWindowScrollTop();
        calcWindowParams();
    };

    gridder.initGridPage1 = function(params){

        gridder.dispatch(params);

        if ( storage.breakpoint ){
            storage.breakpoint = 960;
        }

        if ( storage.cellSize ){
            storage.cellSize = 4;
        }

        if ( storage.colCount ){
            storage.colCount = 24;
        }

        if ( storage.rowsCount ){
            storage.rowsCount = 1000;
        }

        gridder.renderGrid();

        eventer.on('resizeWindow.pageGrid1', gridder.renderGrid);

        $('.gridder-post-grid').on('click', '.gridder-post', function(event) {
            event.preventDefault();

            gridder.viewerGrid1 = new GridderViewer({
                data: {
                    type: 'photo',
                    src: this.querySelector('img').src,
                    id: 12,
                    gridId: 1,
                    mode: GridderViewer.VIEWER_MODE_HORIZON
                }
            })
        });
    };

    gridder.renderGrid = function(){
        gridder.getRate();
        gridder.makeGrid();
    };

    gridder.getRate = function(){
        if (storage.windowWidth <= storage.breakpoint) {
            storage.gridRate = 2;
        } else if(storage.windowWidth > storage.breakpoint) {
            storage.gridRate = 1;
        }
    };

    gridder.makeGrid = function(){
        var $posts = $(".gridder-post");
        var posts = [];
        var rows = [];
        var row = [];
        var freePosition;
        var maxR = 0;
        var index;

        /* расчитываем реальные размеры карточек и добавляем их в массив */
        $posts.each(function() {
            var $post = $(this);

            posts.push({
                node: this,
                w: $post.data('w') * storage.gridRate,
                h: $post.data('h') * storage.gridRate
            });
        });

        /* Создаём структуру заполненности грида */
        for (var rowNum = 0; rowNum < storage.rowsCount; rowNum++) {
            row = [];

            for (var colNum = 0; colNum < storage.colCount; colNum++) {
                row.push(false);
            }

            rows.push(row);
        }

        function checkPosition(rowNum, colNum, w, h) {
            if (colNum + w > storage.colCount) {
                return false;
            }

            for (var row = rowNum; row < rowNum + h; row++) {
                for (var col = colNum; col < colNum + w; col++) {
                    if (rows[row][col]) {
                        return false;
                    }
                }
            }

            return true;
        }

        function getFreePosition(w, h) {
            for (var rowNum = 0; rowNum < storage.rowsCount; rowNum++) {
                for (var colNum = 0; colNum < storage.colCount; colNum++) {
                    if (
                        !rows[rowNum][colNum] &&
                        checkPosition(rowNum, colNum, w, h)
                    ) {
                        return {
                            rowNum: rowNum,
                            colNum: colNum
                        }
                    }
                }
            }

            return {
                rowNum: rowNum,
                colNum: colNum
            }
        }

        function reservePosition(rowNum, colNum, w, h) {
            for (var row = rowNum; row < rowNum + h; row++) {
                for (var col = colNum; col < colNum + w; col++) {
                    rows[row][col] = true;
                }
            }
        }

        for ( index in posts ){
            var post = posts[index];

            freePosition = getFreePosition(post.w, post.h);

            post.r = freePosition.rowNum;
            post.c = freePosition.colNum;

            reservePosition(post.r, post.c, post.w, post.h);

            posts[index] = post;

            if (maxR < post.r + post.h) {
                maxR = post.r + post.h;
            }
        }

        var transformStyle;

        for ( index in posts ){
            transformStyle = [
                'translateY(' + storage.cellSize * posts[index]['r'] + 'vw' + ')',
                'translateX(' + storage.cellSize * posts[index]['c'] + 'vw' +')'
            ];

            gridder.transformElement(posts[index].node, transformStyle.join(' '));

            // $(posts[index].link).css({
            //     'left' : storage.cellSize * posts[index]['c'] + 'vw',
            //     'top' : storage.cellSize * posts[index]['r'] + 'vw' ,
            // })
        }

        var $postGrid = $('.gridder-post-grid');

        $postGrid.css({
            'height': maxR * storage.cellSize + 10 + 'vw',
            'width': storage.colCount * storage.cellSize + 'vw'
        });
    };

    gridder.initGridPage2 = function(params){
        gridder.dispatch(params);

        $('.horizontal-photos-group-wrapper').on('click', '.post-horizontal', function(event) {
            event.preventDefault();

            gridder.viewerGrid1 = new GridderViewer({
                data: {
                    type: 'photo',
                    src: this.src,
                    id: 12,
                    gridId: 1,
                    mode: GridderViewer.VIEWER_MODE_HORIZON
                }
            })
        });
    };

    gridder.initGridPage3 = function(params){
        gridder.dispatch(params);

        $('.gridder-layout').on('click', '.gridder-layout-cell', function(event) {
            event.preventDefault();

            gridder.viewerGrid1 = new GridderViewer({
                data: {
                    type: 'photo',
                    src: '/img/wed-photograf/unnamed (1).jpg',
                    id: 12,
                    gridId: 1,
                    mode: GridderViewer.VIEWER_MODE_HORIZON
                }
            })
        });
    };

    var cssPrefixes = ['', 'O', 'ms', 'Moz', 'Webkit'];

    /**
        * Apply css transform styles to element
        * @param element
        * @param transformStyle
        */
    gridder.transformElement = function(element, transformStyle){
        for (var i = 0; i < cssPrefixes.length; ++i) {
            element.style[cssPrefixes[i] + (cssPrefixes[i] ? 'Transform' : 'transform')] = transformStyle;
        }
    }
})(gridder);

/**
 * popup module
 *
 * for new popup call gridder.popup.open(html, params)
 * @params.width - width popup
 * @params.msg - info message
 * @params.onClose - function after closing
 * @params.onOpen - function after closing
 * @params.wrapperClass- class for wrapper
 *
 * return instance of popup
 */
gridder.popup = function(){};

(function (pop, _proto) {
    pop.stack = [];

    $.extend(pop, new Eventer());

    pop.props = {
        width: 'auto',
        showTop: true,
        onlyContent: true
    };

    pop.on('close', function (event, data) {
        if ( !pop.stack.length ) {
            gridder.nodes.$wrapper.removeClass('fixed');
            gridder.nodes.$wrapper.removeClass('with-scroll');
            gridder.nodes.$window.scrollTop(-document.querySelector('.gridder-wrapper').style.top.replace('px', ''));
            gridder.nodes.$wrapper.css('top', 0);

            pop.unBindEvents();
        } else {
            pop.enableFirst();
        }
    });

    pop.on('open', function (event, data) {
        if (data.context.index === 0) {
            gridder.nodes.$wrapper
                .css('top', -$(window).scrollTop())
                .addClass('fixed');

            gridder.nodes.$window.scrollTop(0);
        }

        if ( pop.stack.length ) {
            pop.enableFirst();
        }
    });

    pop.enableFirst = function () {
        var _stackLength = pop.stack.length;

        for (var i in pop.stack) {
            pop.stack[i].$box[(+i === _stackLength - 1 ? 'remove' : 'add') + 'Class']('invisible');
        }
    };

    pop.open = function (html, params) {
        params = params || {};

        var _ex = new pop(params);

        _ex.html = html;
        _ex.index = pop.stack.length;

        pop.stack.push(_ex);

        params.wrapperClass = params.wrapperClass || '';

        _ex.props = $.extend({}, pop.props, params);

        _ex.open();

        pop.bindEvents();

        pop.emit('open', { context: _ex });

        return _ex;
    };

    pop.bindEvents = function(){
        gridder.nodes.$window.bind('keyup.popup', function(event){
            if ( event.keyCode === 27 ){
                pop.close();
            }
        });
    };

    pop.unBindEvents = function(){
        gridder.nodes.$window.unbind('keyup.popup');
    };

    pop.close = function (index) {
        if (typeof (index) === 'undefined') {
            index = pop.stack.length - 1;
        }

        if (pop.stack[index]) {
            pop.stack[index].close();
        }

        pop.emit('close');
    };

    pop.closeAll = function () {
        for (var i in pop.stack) {
            pop.stack[i].$wrapper.remove();
        }

        pop.stack = [];
    };

    pop.confirm = function (msg, title, callOk, callNo) {
        msg = '' +
            '<div>' + msg + '</div>' +
            '<div>' +
                '<span class="qroom-btn gridder-popup-confirm-yes">Yes</span>&nbsp;&nbsp;&nbsp;' +
                '<span class="qroom-btn gridder-popup-confirm-no">No</span>' +
            '</div>';

        pop.open(msg, {
            title: title,
            width: 'auto',
            onOpen: function () {
                $('.gridder-popup-confirm-yes').click(function () {
                    if (callOk) callOk();

                    pop.close();
                });

                $('.gridder-popup-confirm-no').click(function () {
                    if (callNo) callNo();

                    pop.close();
                });
            }
        })
    };

    pop.info = function (params) {
        params = params || {};

        pop.open(params.msg ? params.msg : '',
            {
                width: params.width || 'auto',
                onClose: function () {
                    if (params.onClose) params.onClose();
                }
            });
    };

    _proto.open = function () {
        var context = this;

        context.render();

        if ( context.props.title ){
            context.$header.html(context.props.title);
        }

        context.$content.html(context.html);
        context.$box.css('maxWidth', context.props.width);

        context.align();

        if (context.props.onOpen) {
            context.props.onOpen(context);
        }

    };

    _proto.align = function () {
        var context = this;
        var boxHeight = context.$box.height();
        var windowHeight = gridder.storage.windowHeight;
        var top = boxHeight >= windowHeight ? 0: ( windowHeight - boxHeight) / 2;

        context.$box.css({ 'margin-top': top }).addClass('opened');
    };

    _proto.close = function () {
        var context = this;

        context.$box
            .css({
                'marginTop': 0
            })
            .addClass('closed');

        setTimeout(function () {
            context.$wrapper.remove();

            pop.stack.splice(context.index, 1);

            for (var i in pop.stack) {
                pop.stack[i].index = i;
            }

            pop.emit('close');

            if (context.props.onClose) {
                context.props.onClose(context);
            }
        }, 200);
    };

    _proto.setContentHTML = function (html) {
        var context = this;

        context.$box.find('.gridder-popup-content').html(html);

        context.html = html;
    };

    _proto.render = function () {
        var context = this;

        context.$wrapper = $('<div>', {
            'id': 'gridder-popup-wrap-' + context.index,
            'class': 'gridder-popup-wrap ' + context.props.wrapperClass
        }).click(function () { pop.close(); });

        context.$box = $('<div>', { 'class': 'gridder-popup-box ' })
            .appendTo(context.$wrapper)
            .click(function (event) { event.stopPropagation(); });

        if ( context.props.title ){
            context.$header = $('<div>', { 'class': 'gridder-popup-header' }).appendTo(context.$box);
        }

        context.$closer = $('<div>', { 'class': 'gridder-popup-close' })
            .appendTo(context.$wrapper)
            .click(function () { pop.close(); });

        context.$content = $('<div>', { 'class': 'gridder-popup-content' }).appendTo(context.$box);

        context.$wrapper.css({ 'z-index': 1000 + context.index });

        gridder.nodes.$heap.append(context.$wrapper);
    };
}(gridder.popup, gridder.popup.prototype));

/**
 *  constructor for viewer photo, video, iframe
 *  @params.data  - Object which contain type of view content
 *  @params.data.type -  {String} 'photo', 'video', 'iframe'
 *  @params.data.src - {String} src of img
 *  @params.data.id  - {Int} id of content
 *  @params.data.gridId - {Int}  id of  grid
 *  @params.data.mode - {String} [GridderViewer.VIEWER_MODE_HORIZON, GridderViewer.VIEWER_MODE_VERTICAL]
 *
 * return viewer instance
 *
 *  in ptototype:
 *  open,
 *  initSkeleton
 *  makeMode
 *  loadData
 *  bindEvents
 *  loadAdditional
 *  changeDisplayProperties
 *  getViewerOrientation
 *  loadAdditional
 *  renderAdditional
 *  cleanBox
 *  next,
 *  prev
 *
 */
var GridderViewer = function(params){
    var ins = this;

    params = params || {};

    if ( !params.data ){
        console.log('contentData is empty');

        return;
    }

    ins.open(params);

    return ins;
};

(function(viewer, _proto){
    viewer.VIEWER_MODE_VERTICAL = 1;
    viewer.VIEWER_MODE_HORIZON = 2;

    var tpl = '' +
        '<div class="gridder-viewer-wrapper nclear">' +
            '<div class="gridder-viewer-display">' +
                '<div class="gridder-viewer-display-pointer next"></div>' +
                '<div class="gridder-viewer-display-pointer prev"></div>' +
            '</div>' +
            '<div class="gridder-viewer-additional">' +
                '<div class="gridder-viewer-additional-info">Likes</div>' +
                '<div class="gridder-viewer-additional-comments">Comments</div>' +
            '</div>' +
        '</div>';

    _proto.open = function(params){
        var context = this;

        if ( !params.data ){
            console.log('wrong way, no data');

            return;
        }

        context.data = params.data;

        context
            .createPopup()
            .initSkeleton()
            .makeMode()
            .loadData()
            .bindEvents()
            .loadAdditional();

        return context;
    };

    _proto.createPopup = function(){
        var context = this;

        if ( !context.skeletonInited ){
            context.popupIns = gridder.popup.open(tpl, {
                wrapperClass: 'gridder-viewer',
                onClose: function(){
                    context.unBindEvents();
                }
            });
        }

        return context;
    };

    _proto.initSkeleton = function(){
        var context = this;

        if ( !context.skeletonInited ){
            context.$wrapper = $('.gridder-viewer-wrapper');
            context.$display = $('.gridder-viewer-display');
            context.$pointerPrev = $('.gridder-viewer-display-pointer.prev');
            context.$pointerNext = $('.gridder-viewer-display-pointer.next');
            context.$additional = $('.gridder-viewer-additional');
            context.$additionalInfo = $('.gridder-viewer-additional-info');
            context.$additionalComments = $('.gridder-viewer-additional-comments');
        }

        context.skeletonInited = true;

        return context;
    };

    _proto.makeMode = function(){
        var context = this;
        var orientation = context.getViewerOrientation();

        switch(orientation) {
            case viewer.VIEWER_MODE_HORIZON:
                context.$wrapper.addClass('horizon');

                break;
            case viewer.VIEWER_MODE_VERTICAL:
                context.$wrapper.removeClass('horizon');

                break;
        }

        return context;
    };

    _proto.loadData = function(){
        var context = this;

        switch( context.data.type ){
            case 'photo':
                context.loadDataImg();

                break;
            case 'iframe':
                context.loadDataIframe();

                break;
            case 'video':
                context.loadDataVideo();

                break;
        }

        return context;
    };

    _proto.bindEvents = function(){
        var context = this;

        context.unBindEvents();

        gridder.nodes.$window.bind('resize.viewer', function(){
            context
                .makeMode()
                .changeDisplayProperties();
        });

        context.$pointerNext.bind('click', function(){
            context.next();
        });

        context.$pointerPrev.bind('click', function(){
            context.prev();
        });

        return context;
    };

    _proto.unBindEvents = function(){
        var context = this;

        context.$pointerNext.unbind('click');
        context.$pointerPrev.unbind('click');

        gridder.nodes.$window.unbind('resize.viewer');

        return context;
    };

    _proto.loadDataImg = function(){
        var context = this;
        var img = new Image();

        console.log('loading...');

        img.onload = function(){
            context.contentWidth = img.width;
            context.contentHeight = img.height;

            context.changeDisplayProperties({
                width: img.width,
                height: img.height
            });

            context.$display.append('<div class="gridder-viewer-display-blur" style="background-image: url(\'' + context.data.src + '\')"></div>');
            context.$display.append(img);
        };

        img.src = context.data.src;
        img.classList.add('gridder-viewer-display-img');

        return context;
    };

    _proto.loadDataVideo = function(){
        var context = this;
        var video = document.createElement('video');

        video.src = context.data.src;

        context.$display.append(video);

        return context;
    };

    _proto.loadDataIframe = function(){
        var context = this;
        var iframe = document.createElement('iframe');

        iframe.src = context.data.src;

        context.$display.append(iframe);

        return context;
    };

    _proto.getViewerOrientation = function(){
        var context = this;
        var orientation = viewer.VIEWER_MODE_VERTICAL;

        if ( gridder.storage.windowWidth > 768 && context.data.mode === viewer.VIEWER_MODE_HORIZON ){
            orientation = viewer.VIEWER_MODE_HORIZON;
        }

        return orientation;
    };

    _proto.changeDisplayProperties = function(params){
        var context = this;
        var orientation = context.getViewerOrientation();
        var paddings = 0;
        var widthToHeight;
        var windowWidth = gridder.storage.windowWidth;
        var windowHeight = gridder.storage.windowHeight;

        params = params || {};

        params.width = params.width || context.contentWidth;
        params.height = params.height || context.contentHeight;

        if ( orientation === viewer.VIEWER_MODE_HORIZON ){
            paddings = context.$additional.outerWidth();
        }

        if ( context.width < params.width || !context.width ){
            context.width = params.width;
        }

        if ( context.height < params.height || !context.height){
            context.height = params.height;
        }

        widthToHeight = context.width / params.height;

        if ( widthToHeight >= 1 ){
            isHorizontal = true;
        }

        // первый прогон параметров с подстройкой по первичным данным
        if ( isHorizontal && context.width > windowWidth - paddings ){
            context.width = windowWidth - paddings;
            context.height = context.width / widthToHeight
        } else if ( !isHorizontal && context.height > windowHeight - paddings ){
            context.height = windowHeight - paddings;
            context.width = context.height * widthToHeight
        }

        // повторно проверяем подходит ли под ширину и высоту картинка
        if ( isHorizontal && context.height > windowHeight - paddings ){
            context.height = windowHeight - paddings;
            context.width = context.height * widthToHeight;
        } else if ( !isHorizontal && context.width > windowWidth - paddings ){
            context.width = windowWidth - paddings;
            context.height = context.width / widthToHeight
        }

        context.$display.css({
            width: context.width,
            height: context.height
        });

        setTimeout(function(){
            context.popupIns.align();
        }, 50);

        return context;
    };

    _proto.loadAdditional = function(){
        var context = this;

        if ( context.lcokLoadAdditional ){
            return;
        }

        context.lcokLoadAdditional = true;

        var data = {
            likes: 12,
            comments: {
                list: [
                    {
                        id: 12,
                        name: 'Name 1',
                        ava: '',
                        url: '',
                        text: ''
                    }
                ],
                offset: 20,
                count: 20
            }
        };

        context.renderAdditional(data);

        // $.post('/loadAdditional', {
        //     gridId: context.data.gridId,
        //     contentId: context.data.id
        // }, function(){
        //     context.lcokLoadAdditional = false;
        // });

        return context;
    };

    _proto.renderAdditional = function(){
        var context = this;

        return context;
    };

    _proto.cleanBox = function(){
        var context = this;

        context.$display.find('.gridder-viewer-display-blur').remove();
        context.$display.find('.gridder-viewer-display-img').remove();

        context.$additionalInfo.html('');
        context.$additionalComments.html('');

        return context;
    };

    _proto.next = function(){
        var context = this;

        console.log('on next');

        if ( context.lockChange ){
            return;
        }

        context.lockChange = true;

        context.cleanBox();

        context.open({
            data: context.data
        });

        context.lockChange = false;

        // $.post('/next', {
        //     gridId: context.data.gridId,
        //     contentId: context.data.id
        // }, function(){
        //     context.lockChange = false;
        //     context.cleanBox();
        // });
    };

    _proto.prev = function(){
        var context = this;

        console.log('on prev');

        if ( context.lockChange ){
            return;
        }

        context.lockChange = true;

        context.cleanBox();

        context.open({
            data: context.data
        });

        context.lockChange = false;

        // $.post('/next', {
        //     gridId: context.data.gridId,
        //     contentId: context.data.id
        // }, function(){
        //     context.lockChange = false;
        //     context.cleanBox();
        // });
    };

})(GridderViewer, GridderViewer.prototype);
